using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnBall : MonoBehaviour {

	List<Transform> spawnPoints;
	public Transform bucketSpawn;

	public AudioSource audio;

	public Transform[] balls;
	public Transform[] buckets;
	float spawnInterval = 2.5f;
	float burstInterval = 0.25f;
	public Transform LSpawnVent;
	public Transform RSpawnVent;
	public bool Spawning = false;

	ConveyorBelt conveyorBelt;
	public GameObject StartSpawn;
	Animator SpawnAnim;

	public List<int> spawnQueue = new List<int>();

	float timestamp = 0;
	int burstTimer = 0;

	public AudioClip[] spawnSounds;

	// Use this for initialization
	void Start () 
	{
		SpawnAnim = StartSpawn.GetComponent<Animator> ();
		audio = GetComponent<AudioSource> ();
		conveyorBelt = Camera.main.GetComponent<ConveyorBelt> ();

		bucketSpawn.position = new Vector3(Camera.main.ScreenToWorldPoint (new Vector2 (0, 0)).x - 1,bucketSpawn.position.y,0);
		timestamp = Time.time + 2;
		spawnPoints = new List<Transform> ();
		foreach(Transform child in transform)
		{
			spawnPoints.Add(child);
		}

		for( int i = 0; i < 6; i++)
		{
			spawnQueue.Add(Random.Range(0,balls.Length));
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.time > timestamp) 
		{;

			Spawn ();
			
			timestamp = Time.time + spawnInterval;
			StartCoroutine (conveyorBelt.Halt(spawnInterval / 2));
			StartCoroutine(StartPipeAnimate());

			
//			burstTimer++;
//			if(burstTimer == 3)
//			{
//				timestamp = Time.time + spawnInterval;
//				burstTimer = 0;
//				StartCoroutine(Bucket.Halt(spawnInterval/3));
//
//			}
//			else 
//			{
//				timestamp = Time.time + burstInterval;
//			}

//		
		
		}
	}


	void Spawn()
	{
		int index = Random.Range (0, balls.Length);

		if (Game.mode == Game.Mode.doubleSpawn){
			for (int i=0; i<2; i++){
				spawnQueue.Add(index);
				Transform spawnedBall = Instantiate (balls [spawnQueue[0]], spawnPoints [Random.Range (0, spawnPoints.Count)].position, Quaternion.identity) as Transform;
				spawnedBall.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -1, 0);
				GetComponent<AudioSource>().PlayOneShot(spawnSounds[Random.Range(0,spawnSounds.Length)]);
				Instantiate (buckets [spawnQueue[0]], bucketSpawn.position + new Vector3(Random.Range(-0.75f*spawnInterval,0.75f*spawnInterval),0,0), Quaternion.identity);
				RSpawnVent.GetComponent<SpriteRenderer> ().color = balls [spawnQueue [1]].GetComponent<Renderer> ().sharedMaterial.color;
				LSpawnVent.GetComponent<SpriteRenderer> ().color = balls [spawnQueue [2]].GetComponent<Renderer> ().sharedMaterial.color;
				
				spawnQueue.RemoveAt (0);
				StartCoroutine ("EndSpawn");
			}
		}

		if (Game.mode == Game.Mode.normal)
		{
			spawnQueue.Add(index);
			Transform spawnedBall = Instantiate (balls [spawnQueue[0]], spawnPoints [Random.Range (0, spawnPoints.Count)].position, Quaternion.identity) as Transform;
			spawnedBall.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -1, 0);
			Instantiate (buckets [spawnQueue[0]], bucketSpawn.position + new Vector3(Random.Range(-0.25f*spawnInterval,0.25f*spawnInterval),0,0), Quaternion.identity);
			GetComponent<AudioSource>().PlayOneShot(spawnSounds[Random.Range(0,spawnSounds.Length)]);


			RSpawnVent.GetComponent<SpriteRenderer> ().color = balls [spawnQueue [1]].GetComponent<Renderer> ().sharedMaterial.color;
			LSpawnVent.GetComponent<SpriteRenderer> ().color = balls [spawnQueue [2]].GetComponent<Renderer> ().sharedMaterial.color;

			spawnQueue.RemoveAt (0);
		}
		if(Game.mode == Game.Mode.randomSpawn)
		{
			spawnQueue.Add(index);
			Transform spawnedBall = Instantiate (balls [spawnQueue[0]], spawnPoints [Random.Range (0, spawnPoints.Count)].position, Quaternion.identity) as Transform;
			spawnedBall.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, -1, 0);
			Instantiate (buckets [spawnQueue[0]], bucketSpawn.position + new Vector3(Random.Range(-0.25f*spawnInterval,0.25f*spawnInterval),0,0), Quaternion.identity);
			GetComponent<AudioSource>().PlayOneShot(spawnSounds[Random.Range(0,spawnSounds.Length)]);
			if(Random.value > 0.5f)
			{

			}
			
			RSpawnVent.GetComponent<SpriteRenderer> ().color = balls [spawnQueue [1]].GetComponent<Renderer> ().sharedMaterial.color;
			LSpawnVent.GetComponent<SpriteRenderer> ().color = balls [spawnQueue [2]].GetComponent<Renderer> ().sharedMaterial.color;
			
			spawnQueue.RemoveAt(0);
		}

	}


	IEnumerator StartPipeAnimate()
	{
		SpawnAnim.SetBool ("Reload", true);
		yield return new WaitForSeconds (0.917f);
		SpawnAnim.SetBool("Reload", false);

	}
}
