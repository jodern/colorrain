﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreCounter : MonoBehaviour {

	public static int comboCount = 1;
	public static int score = 0;

	public static Text scoreDisplay;

	// Use this for initialization
	void Start () 
	{
		scoreDisplay = GameObject.Find ("ScoreDisplay").GetComponent<Text> ();
		scoreDisplay.text = "Score: " + score.ToString ();
		score = 0;
		comboCount = 0;
	}


	public static void ChangeScore(int deltaScore)
	{
		score += deltaScore;
		scoreDisplay.text = "Score: " + score.ToString ();
	}
}
