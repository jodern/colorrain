﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	RectTransform menuPanelRect;
	public Transform menuPanel, optionsPanel, instructoPanel;
	public float volumvalue;
	AudioSource audio;
	public AudioClip menuMusic;
	public bool Sarter1;
	public Slider musicVolSlider, FXvolSlider;

	public GameObject instructotext1,MenuPanel;
	// Use this for initialization


	void Start () 
	{    
		Sarter1=false;
		instructotext1.SetActive(false);
		optionsPanel.gameObject.SetActive (false);
		menuPanelRect = menuPanel as RectTransform;
		menuPanelRect.sizeDelta = new Vector2(Screen.width, Screen.height);
//
//		RectTransform menuText = menuPanelRect.FindChild ("MainMenu") as RectTransform;
//		menuText.sizeDelta = new Vector2 (Screen.width/4,Screen.height/5);

		if (Application.isWebPlayer || Application.platform  == RuntimePlatform.OSXPlayer)
		{
			GameObject.Find("QuitGameBtn").SetActive(false);
		}


		audio = GetComponent<AudioSource> ();
		StartCoroutine (PlayMusic ());
	}

	IEnumerator starter1()
	{
		instructoPanel.gameObject.SetActive (true);
//		instructotext1.SetActive(true);
		yield return new WaitForSeconds (10f);
		Application.LoadLevel ("game");
	}

	void Update ()
	{
		Settings.volStoreMusic = musicVolSlider.value;
		Settings.volStoreFX = FXvolSlider.value;

		if (Sarter1)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Application.LoadLevel ("game");
			}
		}
	}

	IEnumerator PlayMusic()
	{
		audio.PlayOneShot (menuMusic);
		print (menuMusic.length);
		yield return new WaitForSeconds (menuMusic.length);
		StartCoroutine (PlayMusic ());
	}



	public void ClickStartGameButtonsBtn()
	{
		Sarter1=true;
		StartCoroutine (starter1());
		menuPanel.gameObject.SetActive (false);
	}


	public void ClickBackBtn ()
	{
		menuPanel.gameObject.SetActive (true);
		optionsPanel.gameObject.SetActive (false);
	}

	public void ClickOptionsBtn()
	{   
		optionsPanel.gameObject.SetActive (true);
		menuPanel.gameObject.SetActive (false);
	}

	public void ClickQuitBtn()
	{
		Application.Quit ();
	}
}
