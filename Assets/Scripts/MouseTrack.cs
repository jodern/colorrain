﻿using UnityEngine;
using System.Collections;

public class MouseTrack : MonoBehaviour {

	float fanForce = 0.3f;
	public float radius = 2;
	public float rotSpeed = 10f;
	float minSpeed = 100;
	float maxSpeed = 1000;

	// Use this for initialization
	void Start () 
	{
		transform.localScale = new Vector3 (radius, radius, radius);
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector2 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		transform.position = mousePos;

		if(Input.GetMouseButton(0))
		{
			Collider2D[] ballColls = Physics2D.OverlapCircleAll (transform.position, radius);

			foreach(Collider2D ballCol in ballColls)
			{
				if(ballCol.name.Contains("Ball"))
				{
					Vector2 ballMouseV =  ballCol.transform.position -transform.position;
					float fallOff = 1 - (ballMouseV.magnitude/radius);
					if(fallOff < 0)
						fallOff = 0;
					
					ballCol.GetComponent<Rigidbody2D>().AddForce(ballMouseV.normalized * fallOff * fanForce,ForceMode2D.Impulse);
				}
			}
		}
		if(Input.GetMouseButton(0) && rotSpeed <= maxSpeed)
		{
			rotSpeed+=400*Time.deltaTime;
		}
		
		if (!Input.GetMouseButton(0) && rotSpeed >= minSpeed) 
		{
			rotSpeed-=800*Time.deltaTime;
		}
		transform.Rotate(Vector3.back, rotSpeed * Time.deltaTime);
	}
}
