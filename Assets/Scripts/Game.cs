﻿using UnityEngine;
using System.Collections;


public class Game : MonoBehaviour {
	
	public enum Mode {normal,doubleSpawn,randomSpawn}
	public Mode modeSetter;
	public static Mode mode;

	public AudioClip mainMusic;
	AudioSource audio;


	void Awake()
	{
		mode = modeSetter;
		Cursor.visible = false;
	}

	// Use this for initialization
	void Start () 
	{
		Physics2D.gravity = new Vector2 (0, -20);
		audio = GetComponent<AudioSource> ();
		audio.volume = Settings.volStoreMusic;
		StartCoroutine (PlayMusic ());
	}
	
	// Update is called once per frame
	void OnDestroy()
	{
		Cursor.visible = true;
	}

	IEnumerator PlayMusic()
	{
		audio.PlayOneShot (mainMusic);
		yield return new WaitForSeconds (mainMusic.length);
		StartCoroutine (PlayMusic ());
	}
}
