using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class BallController : MonoBehaviour {

	Rigidbody2D rigidbody2D;
	public GameObject Splat;

	public AudioClip[] grabSounds;
	public AudioClip[] hitBeltSounds;
	public AudioClip[] bucketHits;


	// Use this for initialization
	void Start () 
	{
		rigidbody2D = GetComponent<Rigidbody2D> ();
	}

	void DoSplat()
	{
		AudioSource.PlayClipAtPoint(hitBeltSounds[Random.Range(0,hitBeltSounds.Length)],transform.position,Settings.volStoreFX);
		Destroy (gameObject);
		Instantiate (Splat,transform.position,Quaternion.identity);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.name.Contains("Bucket"))
		{
			if(col.gameObject.GetComponent<Bucket>().hasPaint || col.gameObject.tag != gameObject.tag)
			{
				DoSplat();
				ScoreCounter.comboCount = 1;
			}
			else 
			{
				GetComponent<AudioSource>().PlayOneShot (bucketHits [Random.Range (0, bucketHits.Length)]);
				
				//col.gameObject.GetComponent<Renderer>().material.color = Color.black;
				col.gameObject.GetComponent<Bucket>().hasPaint = true;
				ScoreCounter.comboCount++;
				Destroy(gameObject);
			}
		}

		if (col.gameObject.tag == "ConvBelt" )
		{
			ScoreCounter.comboCount = 1;
			DoSplat();
		}
	}
}

