﻿using UnityEngine;
using System.Collections;

public class ConveyorBelt : MonoBehaviour {


	public static float speed = 2.5f;
	public Animator[] conveyorAnims;

	public IEnumerator Halt(float time)
	{
		yield return new WaitForSeconds (2);
		float originalSpeed = speed;
		speed = 0;
		for(int i = 0; i < conveyorAnims.Length; i++)
		{
			conveyorAnims[i].speed = 0;
		}
		yield return new WaitForSeconds (time);
		speed = originalSpeed;
		for(int j = 0; j < conveyorAnims.Length; j++)
		{
			conveyorAnims[j].speed = 1;
		}
	}
}
