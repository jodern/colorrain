﻿using UnityEngine;
using System.Collections;

public class DestroySplat : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (DestroyAfterSeconds(1.5f));
	}
	
	// Update is called once per frame
	void Update () {
	

	}

	IEnumerator DestroyAfterSeconds(float time)
	{
		yield return new WaitForSeconds (time);
		Destroy (gameObject);
	}
}
