﻿using UnityEngine;
using System.Collections;

public class Bucket : MonoBehaviour {

	public bool hasPaint = false;
	Transform checkBucket;
	public Sprite BucketNoPaint;
	public Sprite BucketPaint;
	private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
//		if (spriteRenderer.sprite == null) 
//			spriteRenderer.sprite = BucketNoPaint;
		checkBucket = GameObject.Find ("CheckBucket").transform;
		checkBucket.position = new Vector3( Camera.main.ScreenToWorldPoint (new Vector2(Screen.width,0)).x + 1,checkBucket.position.y,0);
	}
	
	// Update is called once per frame
	void Update () 
	{ 
		transform.position += new Vector3 (ConveyorBelt.speed * Time.deltaTime, 0,0);

		if (hasPaint == true) {
			FillBucket();
			}

		if(Vector2.Distance(transform.position,checkBucket.position) < 0.1f)
		{
			if(!hasPaint)
				ScoreCounter.ChangeScore(-ScoreCounter.comboCount);
			else 
				ScoreCounter.ChangeScore(ScoreCounter.comboCount);
			
			Destroy(gameObject);
		}
	}
	void FillBucket()
	{
		if (spriteRenderer.sprite == BucketNoPaint) // if the spriteRenderer sprite = sprite1 then change to sprite2
		{
			spriteRenderer.sprite = BucketPaint;
		}
	}
}
