﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pause : MonoBehaviour {

	public bool isPaused = false;
	float timeScale;
	public GameObject pauseMenu;

	public GameObject ToogleMusics,ToggleBacksound;
	Button mainMenuButton;
	public Slider FXSlider,musicSlider;
	SpriteRenderer cursor;

//mySlider: UnityEngine.UI.Slider;
	// Use this for initialization
	void Start () 
	{

		cursor = GameObject.Find ("Cursor").GetComponent<SpriteRenderer> ();
		FXSlider.value = Settings.volStoreFX;
		musicSlider.value = Settings.volStoreMusic;
		
		pauseMenu.SetActive (false);
		timeScale = Time.timeScale;


	}

	// Update is called once per frame
	void Update () 
	{
		if(isPaused)
		{
			Settings.volStoreMusic = musicSlider.value;
			Settings.volStoreFX = FXSlider.value;
		}
		if (Application.isWebPlayer) {
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				TogglePause();
			}


		}
		else
		{
			if(Input.GetKeyDown(KeyCode.Escape))
			{
				TogglePause();
			}
		}
	
	}

	void TogglePause()
	{
		isPaused = !isPaused;
		pauseMenu.SetActive (isPaused);
		Time.timeScale = timeScale - Time.timeScale;
		AudioListener.volume = 1.3f - AudioListener.volume;
		Cursor.visible = isPaused;
		cursor.enabled = !isPaused;
	} 



	public void MainMenuBtnClick()
	{
		print ("click main menu button");
		Application.LoadLevel ("Main Menu");
	}
}
