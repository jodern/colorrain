﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndGame : MonoBehaviour {

	public bool won,lost;
	bool runOnce;
	public GameObject winScreen,loseScreen,scoreTimerDisplay;
	private float timedown;
	public int maxTime, timeLeft;
	public Text timerText;

	public GameObject pauseMenu;

	Pause pause;

	// Use this for initialization
	void Start () 
	{
		pause = GameObject.Find ("Pause").GetComponent<Pause> ();
		
		won=false;
		lost=false;
		runOnce = true;

		scoreTimerDisplay.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.G)){lost=true;}



			if (!pause.isPaused && !won && !lost || BucketOrders.stoppcount==true){

			timedown += Time.unscaledDeltaTime;
			
			timeLeft = (int)(maxTime - timedown);

//			timer += Time.unscaledDeltaTime;
//
//			rounder = (int)timer;
//			timeLeft = maxTime - rounder;
		}

		timerText.text = timeLeft.ToString ();

		if (timeLeft < 0 && !won && !lost)
		{
			print ("time's up!");
			if (ScoreCounter.score > 5)
			{
				won = true;
			}
			else if (ScoreCounter.score <= 5)
			{
				lost = true;
			}	
		}
		
		
		if (ScoreCounter.score < -5)
		{
			print ("Too low score, you lose");
			lost = true;
		}
		
		if (lost && runOnce)
		{
			print ("lost the game!");
		
			scoreTimerDisplay.SetActive(false);
			pauseMenu.SetActive (false);
			loseScreen.SetActive(true);
			Time.timeScale = 0;
			runOnce = false;

			StartCoroutine (ReturnMainMenu());
		}
		if (won || lost) 
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Application.LoadLevel ("main menu");
			}
		}

		if (won && runOnce)
		{	
			print ("won the game!");
			scoreTimerDisplay.SetActive(false);
			Time.timeScale = 0;
			pauseMenu.SetActive (false);
			winScreen.SetActive(true);
			runOnce = false;

			StartCoroutine (ReturnMainMenu());

		}

	}

	IEnumerator ReturnMainMenu()
	{
		print ("Returning to main menu");
		yield return new WaitForSeconds (5);
		print ("loading main menu");
		Application.LoadLevel ("main menu");
	}
}
